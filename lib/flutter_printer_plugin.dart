
import 'flutter_printer_plugin_platform_interface.dart';

class FlutterPrinterPlugin {
  //其他所有方法在MethodChannelFlutterPrinterPlugin中
  Future<String?> getPlatformVersion() {
    return FlutterPrinterPluginPlatform.instance.getPlatformVersion();
  }

  Future<String?> isSupportPrinter() {
    return FlutterPrinterPluginPlatform.instance.isSupportPrinter();
  }

  Future<String?> init() {
    return FlutterPrinterPluginPlatform.instance.init();
  }

  Future<String?> free() {
    return FlutterPrinterPluginPlatform.instance.free();
  }

  Future<String?> initPrint() {
    return FlutterPrinterPluginPlatform.instance.initPrint();
  }

  Future<String?> sendData(String content) {
    return FlutterPrinterPluginPlatform.instance.sendData(content);
  }

  Future<String?> printText(String content,String charsetName) {
    return FlutterPrinterPluginPlatform.instance.printText(content,charsetName);
  }

  Future<String?> print(String base64,int mode,int interval,int width,int height) {
    return FlutterPrinterPluginPlatform.instance.print(base64,mode,interval,width,height);
  }

  Future<String?> printImage(String base64,int width,int height) {
    return FlutterPrinterPluginPlatform.instance.printImage(base64,width,height);
  }

  Future<String?> printBarcode(String content,String barcodeType) {
    return FlutterPrinterPluginPlatform.instance.printBarcode(content,barcodeType);
  }

  Future<String?> setFeedRow(int type,int n) {
    return FlutterPrinterPluginPlatform.instance.setFeedRow(type,n);
  }

  Future<String?> setGSPMode(int x,int y) {
    return FlutterPrinterPluginPlatform.instance.setGSPMode(x,y);
  }

  Future<String?> setPrintRowSpacing(int spacing) {
    return FlutterPrinterPluginPlatform.instance.setPrintRowSpacing(spacing);
  }

  Future<String?> setPrintLeftMargin(int nl,int nh) {
    return FlutterPrinterPluginPlatform.instance.setPrintLeftMargin(nl,nh);
  }

  Future<String?> setPrintAlignmentMode(int n) {
    return FlutterPrinterPluginPlatform.instance.setPrintAlignmentMode(n);
  }

  Future<String?> setPrintSpeed(int speed) {
    return FlutterPrinterPluginPlatform.instance.setPrintSpeed(speed);
  }

  Future<String?> setPrintChromaLevel(int chroma) {
    return FlutterPrinterPluginPlatform.instance.setPrintChromaLevel(chroma);
  }

  Future<String?> restoreDefault() {
    return FlutterPrinterPluginPlatform.instance.restoreDefault();
  }

  Future<String?> Black_label_On() {
    return FlutterPrinterPluginPlatform.instance.Black_label_On();
  }

  Future<String?> Black_label_Off() {
    return FlutterPrinterPluginPlatform.instance.Black_label_Off();
  }

  Future<String?> Feed_to_Next() {
    return FlutterPrinterPluginPlatform.instance.Feed_to_Next();
  }

  Future<String?> setBarcodeHeight(int height) {
    return FlutterPrinterPluginPlatform.instance.setBarcodeHeight(height);
  }
  Future<String?> setBarcodeWidth(int width) {
    return FlutterPrinterPluginPlatform.instance.setBarcodeWidth(width);
  }

  Future<String?> setBarcodeHRI(int n) {
    return FlutterPrinterPluginPlatform.instance.setBarcodeHRI(n);
  }
  Future<String?> setUnderlineMode(int n) {
    return FlutterPrinterPluginPlatform.instance.setUnderlineMode(n);
  }

  Future<String?> setBoldMode(int n) {
    return FlutterPrinterPluginPlatform.instance.setBoldMode(n);
  }

  Future<String?> setFontDimension(int n) {
    return FlutterPrinterPluginPlatform.instance.setFontDimension(n);
  }

  Future<String?> Restore_factory(int n) {
    return FlutterPrinterPluginPlatform.instance.Restore_factory(n);
  }

  Future<String?> getBaudRate() {
    return FlutterPrinterPluginPlatform.instance.getBaudRate();
  }

  Future<String?> getVersion() {
    return FlutterPrinterPluginPlatform.instance.getVersion();
  }

  Future<String?> getFontVersion() {
    return FlutterPrinterPluginPlatform.instance.getFontVersion();
  }
}

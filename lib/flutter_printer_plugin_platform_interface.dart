import 'package:plugin_platform_interface/plugin_platform_interface.dart';

import 'flutter_printer_plugin_method_channel.dart';

abstract class FlutterPrinterPluginPlatform extends PlatformInterface {
  /// Constructs a FlutterPrinterPluginPlatform.
  FlutterPrinterPluginPlatform() : super(token: _token);

  static final Object _token = Object();

  static FlutterPrinterPluginPlatform _instance = MethodChannelFlutterPrinterPlugin();

  /// The default instance of [FlutterPrinterPluginPlatform] to use.
  ///
  /// Defaults to [MethodChannelFlutterPrinterPlugin].
  static FlutterPrinterPluginPlatform get instance => _instance;

  /// Platform-specific implementations should set this with their own
  /// platform-specific class that extends [FlutterPrinterPluginPlatform] when
  /// they register themselves.
  static set instance(FlutterPrinterPluginPlatform instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }

  Future<String?> getPlatformVersion() {
    throw UnimplementedError('platformVersion() has not been implemented.');
  }

  Future<String?> isSupportPrinter() {
    throw UnimplementedError('init() has not been implemented.');
  }

  Future<String?> init() {
    throw UnimplementedError('init() has not been implemented.');
  }

  Future<String?> free() {
    throw UnimplementedError('init() has not been implemented.');
  }

  Future<String?> initPrint() {
    throw UnimplementedError('initPrint() has not been implemented.');
  }

  Future<String?> sendData(String content) {
    throw UnimplementedError('sendData() has not been implemented.');
  }

  Future<String?> printText(String content,String charsetName) {
    throw UnimplementedError('printText() has not been implemented.');
  }

  Future<String?> print(String base64,int mode,int interval,int width,int height) {
    throw UnimplementedError('print() has not been implemented.');
  }

  Future<String?> printImage(String base64,int width,int height) {
    throw UnimplementedError('printImage() has not been implemented.');
  }

  Future<String?> printBarcode(String content,String barcodeType) {
    throw UnimplementedError('printBarcode() has not been implemented.');
  }

  Future<String?> setFeedRow(int type,int n) {
    throw UnimplementedError('setFeedRow() has not been implemented.');
  }

  Future<String?> setGSPMode(int x,int y) {
    throw UnimplementedError('setGSPMode() has not been implemented.');
  }

  Future<String?> setPrintRowSpacing(int spacing) {
    throw UnimplementedError('setPrintRowSpacing() has not been implemented.');
  }

  Future<String?> setPrintLeftMargin(int nl,int nh) {
    throw UnimplementedError('setPrintLeftMargin() has not been implemented.');
  }

  Future<String?> setPrintAlignmentMode(int n) {
    throw UnimplementedError('setPrintAlignmentMode() has not been implemented.');
  }

  Future<String?> setPrintSpeed(int speed) {
    throw UnimplementedError('setPrintSpeed() has not been implemented.');
  }

  Future<String?> setPrintChromaLevel(int chroma) {
    throw UnimplementedError('setPrintChromaLevel() has not been implemented.');
  }

  Future<String?> restoreDefault() {
    throw UnimplementedError('restoreDefault() has not been implemented.');
  }

  Future<String?> Black_label_On() {
    throw UnimplementedError('Black_label_On() has not been implemented.');
  }

  Future<String?> Black_label_Off() {
    throw UnimplementedError('Black_label_Off() has not been implemented.');
  }

  Future<String?> Feed_to_Next() {
    throw UnimplementedError('Feed_to_Next() has not been implemented.');
  }

  Future<String?> setBarcodeHeight(int height) {
    throw UnimplementedError('setBarcodeHeight() has not been implemented.');
  }
  Future<String?> setBarcodeWidth(int width) {
    throw UnimplementedError('setBarcodeWidth() has not been implemented.');
  }

  Future<String?> setBarcodeHRI(int n) {
    throw UnimplementedError('setBarcodeHRI() has not been implemented.');
  }
  Future<String?> setUnderlineMode(int n) {
    throw UnimplementedError('setUnderlineMode() has not been implemented.');
  }

  Future<String?> setBoldMode(int n) {
    throw UnimplementedError('setBoldMode() has not been implemented.');
  }
  Future<String?> setFontDimension(int n) {
    throw UnimplementedError('setFontDimension() has not been implemented.');
  }
  Future<String?> Restore_factory(int n) {
    throw UnimplementedError('Restore_factory() has not been implemented.');
  }
  Future<String?> getBaudRate() {
    throw UnimplementedError('getBaudRate() has not been implemented.');
  }

  Future<String?> getVersion() {
    throw UnimplementedError('getVersion() has not been implemented.');
  }
  Future<String?> getFontVersion() {
    throw UnimplementedError('getFontVersion() has not been implemented.');
  }


}

import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

import 'flutter_printer_plugin_platform_interface.dart';

/// An implementation of [FlutterPrinterPluginPlatform] that uses method channels.
class MethodChannelFlutterPrinterPlugin extends FlutterPrinterPluginPlatform {
  /// The method channel used to interact with the native platform.
  @visibleForTesting
  final methodChannel = const MethodChannel('flutter_printer_plugin');

  @override
  Future<String?> getPlatformVersion() async {
    return await methodChannel.invokeMethod<String>('getPlatformVersion');
  }

  Future<String?> isSupportPrinter() async{
    return await methodChannel.invokeMethod<String>('isSupportPrinter');
  }

  Future<String?> init() async{
    return await methodChannel.invokeMethod<String>('init');
  }

  Future<String?> free() async{
    return await methodChannel.invokeMethod<String>('free');
  }

  Future<String?> initPrint() async{
    return await methodChannel.invokeMethod<String>('initPrint');
  }

  Future<String?> sendData(String content) async{
    return await methodChannel.invokeMethod<String>('sendData',{'content': content});
  }

  Future<String?> printText(String content,String charsetName) async{
    return await methodChannel.invokeMethod<String>('printText',{'content': content,'charsetName':charsetName});
  }

  Future<String?> print(String base64,int mode,int interval,int width,int height) async{
    return await methodChannel.invokeMethod<String>('print',
        {'base64': base64,'mode':mode,'interval':interval,'width':width,'height':height});
  }

  Future<String?> printImage(String base64,int width,int height) async{
    return await methodChannel.invokeMethod<String>('printImage',{'base64': base64,'width':width,'height':height});
  }

  Future<String?> printBarcode(String content,String barcodeType) async{
    return await methodChannel.invokeMethod<String>('printBarcode',{'content': content,'barcodeType':barcodeType});
  }

  Future<String?> setFeedRow(int type,int n) async{
    return await methodChannel.invokeMethod<String>('setFeedRow',{'type': type,'n':n});
  }

  Future<String?> setGSPMode(int x,int y) async{
    return await methodChannel.invokeMethod<String>('setGSPMode',{'x': x,'y':y});
  }

  Future<String?> setPrintRowSpacing(int spacing) async{
    return await methodChannel.invokeMethod<String>('setPrintRowSpacing',{'spacing': spacing});
  }

  Future<String?> setPrintLeftMargin(int nl,int nh) async{
    return await methodChannel.invokeMethod<String>('setPrintLeftMargin',{'nl': nl,'nh':nh});
  }

  Future<String?> setPrintAlignmentMode(int n) async{
    return await methodChannel.invokeMethod<String>('setPrintAlignmentMode',{'n': n});
  }

  Future<String?> setPrintSpeed(int speed) async{
    return await methodChannel.invokeMethod<String>('setPrintSpeed',{'speed': speed});
  }

  Future<String?> setPrintChromaLevel(int chroma) async{
    return await methodChannel.invokeMethod<String>('setPrintChromaLevel',{'chroma': chroma});
  }

  Future<String?> restoreDefault() async{
    return await methodChannel.invokeMethod<String>('restoreDefault');
  }

  Future<String?> Black_label_On() async{
    return await methodChannel.invokeMethod<String>('Black_label_On');
  }

  Future<String?> Black_label_Off() async{
    return await methodChannel.invokeMethod<String>('Black_label_Off');
  }

  Future<String?> Feed_to_Next() async{
    return await methodChannel.invokeMethod<String>('Feed_to_Next');
  }

  Future<String?> setBarcodeHeight(int height) async{
    return await methodChannel.invokeMethod<String>('setBarcodeHeight',{'height':height});
  }

  Future<String?> setBarcodeWidth(int width) async{
    return await methodChannel.invokeMethod<String>('setBarcodeWidth',{'width':width});
  }

  Future<String?> setBarcodeHRI(int n) async{
    return await methodChannel.invokeMethod<String>('setBarcodeHRI',{'n':n});
  }

  Future<String?> setUnderlineMode(int n) async{
    return await methodChannel.invokeMethod<String>('setUnderlineMode',{'n':n});
  }

  Future<String?> setBoldMode(int n) async{
    return await methodChannel.invokeMethod<String>('setBoldMode',{'n':n});
  }

  Future<String?> setFontDimension(int n) async{
    return await methodChannel.invokeMethod<String>('setFontDimension',{'n':n});
  }

  Future<String?> Restore_factory(int n) async{
    return await methodChannel.invokeMethod<String>('Restore_factory',{'n':n});
  }

  Future<String?> getBaudRate() async{
    return await methodChannel.invokeMethod<String>('getBaudRate');
  }

  Future<String?> getVersion() async{
    return await methodChannel.invokeMethod<String>('getVersion');
  }

  Future<String?> getFontVersion() async{
    return await methodChannel.invokeMethod<String>('getFontVersion');
  }
}
